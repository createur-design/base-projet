    </main>
    <footer>
        <div class="row">
            <div class="small-12 columns">
                <small>copyright <?php echo date("Y"); ?> by JP & Chris</small>
            </div>
        </div>
    </footer>
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/app.min.css">
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="js/app.min.js"></script>
</body>
</html>