<?php
$page = "accueil";
$titre = "Bienvenue"; // à completer pour le SEO
$description = "page d'accueil"; // à completer pour le SEO

include 'inc/header.php';

?>

<!-- Content -->
<div id="avion"><img src="http://pluspng.com/img-png/free-png-hd-planes-airplane-png-clipart-5060.png" alt=""></div>
<section class="hero">

    <div class="row">
        <div class="small-12 columns">
            <small>section 1 Hero</small>
            <h1>Titre unique de la page</h1>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eum, obcaecati nemo. Fugiat nihil, ut laboriosam similique sit enim quia quas tempora amet delectus quo iste labore deserunt dolores laborum sapiente.</p>
            <h2>Intégration d'une image</h2>
            <p><small>grâce à Foundation</small></p>  
        </div> 
    </div>

    <div class="row full">
        <div class="small-8 medium-6 large-6 columns">  
            <div class="colorRed">
                <p></p>
            </div>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nisi at perferendis vel eveniet ullam, adipisci, aliquam incidunt nihil asperiores, itaque nulla. Sint, corporis rem natus veritatis suscipit reprehenderit recusandae. Quidem.</p>      
            <img src="img/4k.jpg" alt="titre">
        </div>
    </div>  
    
    <hr>

    <div class="row align-right align-middle">
        <div class="small-12 small-order-2 medium-4 medium-order-1 columns">
            <img src="https://via.placeholder.com/600x800" alt="mon image">
        </div>
        <div class="small-12 small-order-1 medium-4 medium-order-2 columns">
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quos sit, perspiciatis ipsam perferendis sed praesentium! Eos modi sint quidem laborum nulla incidunt, voluptate voluptas sit fuga natus numquam facere at.</p>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <hr>
        </div>
    </div>

    <div class="row align-center">
        <div class="small-12 columns">
            <h2>Images sur plusieurs colonnes</h2>
        </div>
        <div class="small-6 medium-4 columns">
            <img src="https://via.placeholder.com/800x400" alt="mon image">
        </div>
        <div class="small-6 medium-4 columns">
            <img src="https://via.placeholder.com/800x400" alt="mon image">
        </div>
        <div class="small-6 medium-4 columns">
            <img src="https://via.placeholder.com/800x400" alt="mon image">
        </div>
    </div> 

</section>

<div class="row">
    <div class="small-12 columns">
        <hr>
    </div>
</div>

<section>
    <div class="row">
        <div class="small-12 columns">
            <h2>owl carousel exemple</h2>
            <p>Touch enabled jQuery plugin that lets you create a beautiful responsive carousel slider.</p>
            <a href="https://owlcarousel2.github.io/OwlCarousel2/" target="_blank">link here</a>
        </div>
    </div>
</section>

<section>

    <img src="img/4k.jpg" alt="titre">

</section>

<!-- /Content -->

<?php include 'inc/footer.php'; ?>

