<?php
$page = "objet";
$titre = ""; // à completer pour le SEO
$description = ""; // à completer pour le SEO

include 'inc/header.php';

?>

<!-- Content -->

<section class="objetJS">

    <div class="row">
        <div class="small-12 columns">
            <small>cours</small>
            <H1>JAVASCRIPT</H1>
            <hr>
            <h2>Les objets</h2>            
        </div>
    </div>

    <div class="row">
        <div class="samll-12 columns">
            <p>On connait déjà les tableaux (<b>array</b>) en JS
            <pre>
                var chien = ["choupette", 2, "fauve clair"];
            </pre>
            </p>
            <p>On va évoquer <b>un autre type de variable</b>, les <b>objets</b> constitués de <b>propriétés</b>, et de <b>méthodes</b>.</p>
            <p>Voici la nouvelle syntaxe :
            <pre>
                var dog = {
                    nom: "choupette",
                    age: 2,
                    couleur: "fauve clair",
                    aboie: function(number){
                        while(number>0){
                            console.log("Whouaf !");
                            number--;
                        }
                    }
                };
            </pre>
            </p>
            <p>
            Pour aficher une propriété, par exemple l'age
            <pre>
                console.log(dog.age); //affiche 2
            </pre>
            </p>
            <p>Pour lancer une méthode dans l'objet
            <pre>
                dog.aboie(2) // affiche Whouaf ! Whouaf !
            </pre>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <h2>Les fonctions construteurs</h2>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <p>On a envie de construire une usine à fabriquer des chiens.</p>
            <pre>
                function Dog(nom, age, couleur){
                    this.nom = nom;
                    this.age = age;
                    this.couleur = couleur;
                    this.aboie = function(){
                        console.log("Whouaf !");
                    }
                }
            </pre>
            <p>Pour créer un nouveau chien
            <pre>
                var choupette = new Dog("choupette", 2, "fauve clair");
            </pre>
            </p>
            <p>Pour faire aboyer choupette
            <pre>
                choupette.aboie();
            </pre>
            </p>
        </div>
    </div>

    <script>
    var chien = ["Maya", 2, "fauve clair"];
    //console.log(chien.length);
        var dog = {
            nom: "Maya",
            age: 2,
            couleur: "fauve clair",
            abboie: function(number){
                while(number>0){
                    console.log("whouaf !");
                    number--;
                }
                
            }
        }
        //console.log(dog);
        // for(var propriete in dog){
        //     console.log(dog[propriete]);
        // }
        dog.abboie(2);

        var myFunction = function(){
            console.log("ma fonction classique");
        }
        myFunction();

        const myFunctionFlechee = () => console.log("ma fonction fléchée ! COOL :-)");
        myFunctionFlechee();
        

    </script>

</section>



<!-- /Content -->

<?php include 'inc/footer.php'; ?>